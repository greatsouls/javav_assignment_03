package com.skynetgate.web;

import com.skynetgate.beans.PlayerContainer;
import com.skynetgate.beans.TrainerContainer;
import com.skynetgate.factories.FactoryTrainer;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Emil Gochev.
 */

@RunWith(SpringJUnit4ClassRunner.class)

public class TrainerControllerTest {

//    @Test
//    public void testDisplayTrainerRegistrationForm() throws Exception {
//        TrainerController controller = new TrainerController(new TrainerContainer());
//        MockMvc mockMvc = standaloneSetup(controller).build();
//        mockMvc.perform(get("/trainer/create"))
//                .andExpect(view().name("createTrainerForm"));
//    }

//    @Test
//    public void testRegisterNewTrainer() throws Exception {

//TrainerContainer mockContainer = mock(TrainerContainer.class);
//        TrainerController controller = new TrainerController(mockContainer);


//        TrainerController controller = new TrainerController(new TrainerContainer());
//        MockMvc mockMvc = standaloneSetup(controller).build();
//
//        mockMvc.perform(post("/trainer/create")
//                .param("firstName", "Jack")
//                .param("lastName", "Bauer")
//                .param("age", "50")
//                .param("annualSalary", "1000000"))
//                .andExpect(view().name("createTrainerForm"));
//                .andExpect(redirectedUrl("/trainer/Jack"));

//    }
}
