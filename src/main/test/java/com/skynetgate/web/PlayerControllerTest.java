package com.skynetgate.web;

import com.skynetgate.beans.PlayerContainer;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Emil Gochev.
 */

public class PlayerControllerTest {

    @Test
    public void testDisplayPlayerRegistrationForm() throws Exception {
        PlayerController controller = new PlayerController(new PlayerContainer());
        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(get("/player/create"))
                .andExpect(view().name("createPlayerForm"));
    }


    @Test
    public void testRegisterNewPlayer() throws Exception {

        PlayerController controller = new PlayerController(new PlayerContainer());
        MockMvc mockMvc = standaloneSetup(controller).build();

        mockMvc.perform(post("/player/create")
                .param("firstName", "Jack")
                .param("lastName", "Bauer")
                .param("age", "22")
                .param("countryOfBirth", "Canada")
                .param("position", "DEFENDER")
                .param("annualSalary", "1000000")
                .param("numberOfGoals", "24")
                .param("numberOfBookings", "2"))
                .andExpect(redirectedUrl("/player/DEFENDER"));

    }


}
