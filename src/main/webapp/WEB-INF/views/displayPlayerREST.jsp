<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page session="false" %>
<html>
<head>
    <title>Create Player</title>
    <link rel="stylesheet" type="text/css"
          href="<c:url value="/resources/style.css" />">
</head>
<body>
<h1>Display Player by Last Name</h1>
<sf:form method="POST" modelAttribute="playerContainer">
    <sf:errors path="*" element="div" cssClass="errors"/>

    <table border="0" cellspacing="2">
        <tr>
            <td><spring:message code="last.name"/></td>
            <td><input type="text" name="lastName"/><br/></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="reset" value="<spring:message code="Reset"/>"/> |
                <input type="submit" value="<spring:message code="ShowProfileREST"/>"/> |
                <button type="button" onclick="document.location='/assignment_03/'"><spring:message code="Cancel"/></button>
            </td>
        </tr>
    </table>
</sf:form>
</body>
</html>
