package com.skynetgate.data;

import com.skynetgate.beans.TrainerContainer;
import java.util.List;

/**
 * Created by Emil Gochev.
 */

public interface TrainerRepository {
    TrainerContainer save(TrainerContainer trainerContainer);
    List<TrainerContainer> findAllTrainers();
    List<TrainerContainer> findTrainerByFirstAndLastName(String firstName, String lastName);
}
