package com.skynetgate.data;

import com.skynetgate.beans.PlayerContainer;
import com.skynetgate.beans.PlayerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

/**
 * Created by Emil Gochev.
 */
@Repository
public class JdbcPlayerRepository implements PlayerRepository {

    private JdbcOperations jdbc;

    @Autowired
    public JdbcPlayerRepository(JdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    public PlayerContainer save(PlayerContainer playerContainer) {
        String sql = "INSERT INTO Player (first_name, last_name, age, country, position, salary, goals, bookings)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatementCreatorFactory statementCreatorFactory = new PreparedStatementCreatorFactory(sql, Types.VARCHAR, Types.VARCHAR, Types.INTEGER,
                Types.VARCHAR, Types.VARCHAR, Types.DOUBLE, Types.VARCHAR, Types.VARCHAR);
        statementCreatorFactory.setReturnGeneratedKeys(true);
        PreparedStatementCreator creator = statementCreatorFactory.newPreparedStatementCreator(new Object[]{
                playerContainer.getFirstName(),
                playerContainer.getLastName(),
                playerContainer.getAge(),
                playerContainer.getCountryOfBirth(),
                playerContainer.getPosition(),
                playerContainer.getAnnualSalary(),
                playerContainer.getNumberOfGoals(),
                playerContainer.getNumberOfBookings()
        });

//        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
//        jdbc.update(creator, keyHolder);
        jdbc.update(creator);
        return new PlayerContainer(
//                keyHolder.getKey().intValue(),
                playerContainer.getFirstName(),
                playerContainer.getLastName(),
                playerContainer.getAge(),
                playerContainer.getCountryOfBirth(),
                playerContainer.getPosition(),
                playerContainer.getAnnualSalary(),
                playerContainer.getNumberOfGoals(),
                playerContainer.getNumberOfBookings());
    }

    public List<PlayerContainer> findAllPlayers() {
        return jdbc.query(
                "SELECT first_name, last_name, age, country, position, salary, goals, bookings" +
                        " FROM Player",
                new PlayerRowMapper());
    }

    public List<PlayerContainer> findPlayerByFirstAndLastName(String firstName, String lastName) {
        return jdbc.query(
                "SELECT first_name, last_name, age, country, position, salary, goals, bookings" +
                        " FROM Player" +
                        " WHERE first_name=?" +
                        " AND last_name=?",
                new PlayerRowMapper(),
                firstName, lastName);
    }

    public List<PlayerContainer> findPlayerByLastName(String lastName) {
        try {
            return jdbc.query(
                    "SELECT first_name, last_name, age, country, position, salary, goals, bookings" +
                            " FROM Player" +
                            " WHERE last_name=?",
                    new PlayerRowMapper(), lastName);
        } catch (EmptyResultDataAccessException e) {
            throw new PlayerNotFoundException(lastName);
        }
    }

    private static class PlayerRowMapper implements RowMapper<PlayerContainer> {
        public PlayerContainer mapRow(ResultSet rs, int rowNum) throws SQLException {

            PlayerType playerType;
            String s = rs.getString("position");

            if (s.equals("GOALKEEPER")) {
                playerType = PlayerType.GOALKEEPER;
            } else if (s.equals("DEFENDER")) {
                playerType = PlayerType.DEFENDER;
            } else if (s.equals("MIDFIELDER")) {
                playerType = PlayerType.MIDFIELDER;
            } else {
                playerType = PlayerType.FORWARD;
            }

            PlayerContainer playerContainer = new PlayerContainer(
                    rs.getString("first_name"),
                    rs.getString("last_name"),
                    rs.getInt("age"),
                    rs.getString("country"),
                    playerType,
                    rs.getBigDecimal("salary"),
                    rs.getInt("goals"),
                    rs.getInt("bookings"));
            return playerContainer;
        }
    }
}