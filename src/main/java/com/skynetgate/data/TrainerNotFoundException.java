package com.skynetgate.data;

/**
 * Created by Emil Gochev.
 */
public class TrainerNotFoundException extends RuntimeException  {

    private String firstName;
    private String lastName;

    public  TrainerNotFoundException(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
