package com.skynetgate.data;

import com.skynetgate.beans.TrainerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

/**
 * Created by Emil Gochev.
 */
@Repository
public class JdbcTrainerRepository implements TrainerRepository {

    private JdbcOperations jdbc;

    @Autowired
    public JdbcTrainerRepository(JdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    public TrainerContainer save(TrainerContainer trainerContainer) {
        String sql ="INSERT INTO Trainer (first_name, last_name, age, salary)" +
                        "VALUES (?, ?, ?, ?)";
        PreparedStatementCreatorFactory statementCreatorFactory = new PreparedStatementCreatorFactory(sql, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.DOUBLE);
        statementCreatorFactory.setReturnGeneratedKeys(true);
        PreparedStatementCreator creator = statementCreatorFactory.newPreparedStatementCreator(new Object[] {
                trainerContainer.getFirstName(),
                trainerContainer.getLastName(),
                trainerContainer.getAge(),
                trainerContainer.getAnnualSalary()
        });

//        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
//        jdbc.update(creator, keyHolder);
        jdbc.update(creator);
        return new TrainerContainer(
//                keyHolder.getKey().intValue(),
                trainerContainer.getFirstName(),
                trainerContainer.getLastName(),
                trainerContainer.getAge(),
                trainerContainer.getAnnualSalary());
    }

    public List<TrainerContainer> findAllTrainers() {
        return jdbc.query(
                "SELECT first_name, last_name, age, salary" +
                        " FROM Trainer",
                new TrainerRowMapper());
    }

    public List<TrainerContainer> findTrainerByFirstAndLastName(String firstName, String lastName) {
        try {
            return jdbc.query(
                    "SELECT first_name, last_name, age, salary" +
                            " FROM Trainer" +
                            " WHERE first_name=?" +
                            " AND last_name=?",
                    new TrainerRowMapper(),
                    firstName, lastName);
        } catch (EmptyResultDataAccessException e) {
            throw new TrainerNotFoundException(firstName, lastName);
        }
    }

    private static class TrainerRowMapper implements RowMapper<TrainerContainer> {
        public TrainerContainer mapRow(ResultSet rs, int rowNum) throws SQLException {
            TrainerContainer trainerContainer = new TrainerContainer(
                    rs.getString("first_name"),
                    rs.getString("last_name"),
                    rs.getInt("age"),
                    rs.getBigDecimal("salary"));
            return trainerContainer;
        }
    }
}