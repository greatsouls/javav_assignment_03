package com.skynetgate.data;

import com.skynetgate.beans.PlayerContainer;

import java.util.List;

/**
 * Created by Emil Gochev.
 */
public interface PlayerRepository {

    PlayerContainer save(PlayerContainer playerContainer);
    List<PlayerContainer> findAllPlayers();
    List<PlayerContainer> findPlayerByFirstAndLastName(String firstName, String lastName);
    List<PlayerContainer> findPlayerByLastName(String lastName);
}
