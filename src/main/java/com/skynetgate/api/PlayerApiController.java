package com.skynetgate.api;

import com.skynetgate.beans.PlayerContainer;
import com.skynetgate.data.PlayerNotFoundException;
import com.skynetgate.data.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Emil Gochev.
 */
@RestController
@RequestMapping("/playerapi")
public class PlayerApiController {

    public PlayerRepository playerRepository;

    @Autowired
    public PlayerApiController(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @RequestMapping(value = "/{lastName}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> showCreatedPlayer(@PathVariable String lastName) {
        System.out.println();
        System.out.println("Last name is: " + lastName);
        System.out.println();
        try {
            List<PlayerContainer> playerContainer = playerRepository.findPlayerByLastName(lastName);
            System.out.println(playerContainer);
            if (playerContainer.size() > 0) {
                return new ResponseEntity<List<PlayerContainer>>(playerContainer, HttpStatus.OK);
            } else {
                Error error = new Error(1, "Player with last name [" + lastName + "] not found");
                return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
            }
        } catch (PlayerNotFoundException e) {
            Error error = new Error(1, "Player with last name [" + lastName + "] not found");
            return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
        }
    }
}
