package com.skynetgate.api;

import com.skynetgate.beans.TrainerContainer;
import com.skynetgate.data.TrainerNotFoundException;
import com.skynetgate.data.TrainerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Emil Gochev.
 */

@RestController
@RequestMapping("/trainerapi")
public class TrainerApiController {

    public TrainerRepository trainerRepository;

    @Autowired
    public TrainerApiController(TrainerRepository trainerRepository) {
        this.trainerRepository = trainerRepository;
    }

    @RequestMapping(value = "/{firstNameLastName}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> showCreatedTrainer(@PathVariable String firstNameLastName) {
        String firstName = firstNameLastName.split("_")[0];
        String lastName = firstNameLastName.split("_")[1];
        System.out.println();
        System.out.println("First name is: " + firstName);
        System.out.println("Last name is: " + lastName);
        System.out.println();
        try {
            List<TrainerContainer> trainerContainer = trainerRepository.findTrainerByFirstAndLastName(firstName, lastName);
            System.out.println(trainerContainer);
            if (trainerContainer.size() > 0) {
                return new ResponseEntity<List<TrainerContainer>>(trainerContainer, HttpStatus.OK);
            } else {
                Error error = new Error(1, "Trainer with name [" + firstName + " " + lastName + "] not found");
                return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
            }
        } catch (TrainerNotFoundException e) {
            Error error = new Error(1, "Trainer with name [" + firstName + " " + lastName + "] not found");
            return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
        }
    }
}
