package com.skynetgate.web;

import com.skynetgate.beans.*;
import com.skynetgate.data.TrainerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Emil Gochev.
 */

@Controller
@RequestMapping("/trainer")
public class TrainerController {
    private TrainerRepository trainerRepository;

//    private Currency currency = Currency.getInstance(Locale.US);
//    HttpServletRequest httpServletRequest;
//    private Currency currency = Currency.getInstance(Locale.CANADA);

    @Autowired
    public TrainerController(TrainerRepository trainerRepository) {
        this.trainerRepository = trainerRepository;
    }

    @RequestMapping(value = "/create", method = GET)
    public String showRegistrationForm(Model model) {
        model.addAttribute(new TrainerContainer());
        return "createTrainerForm";
    }

    @RequestMapping(value = "/create", method = POST)
    public String processCreateTrainer(
            @Valid TrainerContainer trainerContainer,
            Errors errors) {
        if (errors.hasErrors()) {
            return "createTrainerForm";
        }
        trainerRepository.save(trainerContainer);
        return "redirect:/trainer/" + trainerContainer.getFirstName() + "_" + trainerContainer.getLastName();
    }

    @RequestMapping(value = "/{firstNameAndLast}", method = GET)
    public String showCreatedTrainer(@PathVariable String firstNameAndLast, Model model) {

        String firstName = firstNameAndLast.split("_")[0];
        String lastName = firstNameAndLast.split("_")[1];
        System.out.println();
        System.out.println("First name is: " + firstName);
        System.out.println("Last name is: " + lastName);
        System.out.println();
        List<TrainerContainer> trainerContainer = trainerRepository.findAllTrainers();
        model.addAttribute("trainerContainer", trainerContainer);
        System.out.println(model.toString());
        return "profileTrainer";
    }

    @RequestMapping(value = "/displayUsingREST", method = GET)
    public String getTrainerREST(Model model) {
        model.addAttribute(new TrainerContainer());
        return "displayTrainerREST";
    }

    @RequestMapping(value = "/displayUsingREST", method = POST)
    public String displayTrainerREST(@RequestParam("firstName") String firstName,
                                     @RequestParam("lastName") String lastName,
                                     Map<String, Object> map, HttpServletRequest request) {
        if ((firstName == "") || (firstName == null) || (lastName == "") || (lastName == null)) {
            System.out.println("first and / or last names are empty");
            return "displayTrainerREST";
        }
        return "redirect:/trainerapi/" + firstName + "_" + lastName;
    }
}
