package com.skynetgate.web;

import com.skynetgate.beans.PlayerContainer;
import com.skynetgate.data.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Emil Gochev.
 */

@Controller
@RequestMapping("/player")
public class PlayerController {
    private PlayerRepository playerRepository;

    @Autowired
    public PlayerController(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @RequestMapping(value = "/create", method = GET)
    public String showRegistrationForm(Model model) {
        model.addAttribute(new PlayerContainer());
        return "createPlayerForm";
    }

    @RequestMapping(value = "/create", method = POST)
    public String processCreatePlayer(
            @Valid PlayerContainer playerContainer,
            Errors errors) {
        if (errors.hasErrors()) {
            return "createPlayerForm";
        }
        playerRepository.save(playerContainer);
        return "redirect:/player/" + playerContainer.getLastName();
    }

    @RequestMapping(value = "/{lastName}", method = GET)
    public String showCreatedPlayer(@PathVariable String lastName, Model model) {
        System.out.println();
        System.out.println("Last name is: " + lastName);
        System.out.println();
        List<PlayerContainer> playerContainer = playerRepository.findAllPlayers();
        model.addAttribute("playerContainer", playerContainer);
        System.out.println(model.toString());
        return "profilePlayer";
    }

    @RequestMapping(value = "/displayUsingREST", method = GET)
    public String getPlayerREST(Model model) {
        model.addAttribute(new PlayerContainer());
        return "displayPlayerREST";
    }

    @RequestMapping(value = "/displayUsingREST", method = POST)
    public String displayPlayerREST(@RequestParam("lastName") String lastName,
                                    Map<String, Object> map, HttpServletRequest request) {
        if ((lastName == "") || (lastName == null)) {
            System.out.println("last names is empty");
            return "displayPlayerREST";
        }
        return "redirect:/playerapi/" + lastName;
    }
}
