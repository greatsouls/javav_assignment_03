package com.skynetgate.factories;

import com.skynetgate.beans.Player;
import com.skynetgate.beans.Team;
import com.skynetgate.beans.Trainer;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Emil Gochev.
 */

@Component
public class FactoryTeam {

    private List<Player> playerList;
    private Trainer trainer;
    private String name;
    private int yearOfFoundation;

    public Team createTeam(List<Player> playerList, Trainer trainer, String name, int yearOfFoundation) {
        this.playerList = playerList;
        this.trainer = trainer;
        this.name = name;
        this.yearOfFoundation = yearOfFoundation;

        if (isValidTeam()) {
            return new Team(this.playerList, this.trainer, this.name, this.yearOfFoundation);
        } else {
            System.out.println("Team was NOT created");
            return null;
        }
    }

    private boolean isValidTeam() {
        if (isValidPlayerList() && isValidTrainer() && isValidStr(name) && isValidYearOfFoundation()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isValidPlayerList() {
        if (playerList.size() == 22) {
            return true;
        } else {
            System.out.println("\nIncorrect argument for player List. Expected number is 22");
            return false;
        }
    }

    private boolean isValidTrainer() {
        if (trainer != null) {
            return true;
        } else {
            System.out.println("\nIncorrect argument for Trainer");
            return false;
        }
    }

    private boolean isValidStr(String str) {
//validates name
        if (!str.isEmpty()) {
            return true;
        } else {
            System.out.println("\nIncorrect argument for Team name");
            return false;
        }
    }

    private boolean isValidYearOfFoundation() {
        if (yearOfFoundation >= 1950) {
            return true;
        } else {
            System.out.println("\nIncorrect argument for year of Foundation. Expected number >= 1950");
            return false;
        }
    }
}
