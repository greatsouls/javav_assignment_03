package com.skynetgate.beans;

import org.springframework.stereotype.Component;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;

/**
 * Created by Emil Gochev.
 */

@Component
public class TrainerContainer {

    @NotNull
    @Size(min=2, max=30)
    private String firstName;

    @NotNull
    @Size(min=2, max=30)
    private String lastName;

    @NotNull
    @Min(value = 40)
    private int age;

    @NotNull
    private BigDecimal annualSalary;

    private Currency currency = Currency.getInstance(Locale.CANADA);

    public TrainerContainer() {
    }

    public TrainerContainer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public TrainerContainer(String firstName, String lastName, int age, BigDecimal annualSalary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.annualSalary = annualSalary;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public BigDecimal getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(BigDecimal annualSalary) {
        this.annualSalary = annualSalary;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TrainerContainer)) return false;
        TrainerContainer that = (TrainerContainer) o;
        return Objects.equals(age, that.age) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(annualSalary, that.annualSalary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, age, annualSalary);
    }

    @Override
    public String toString() {
        return "TrainerContainer{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", annualSalary=" + annualSalary +
                '}';
    }
}
