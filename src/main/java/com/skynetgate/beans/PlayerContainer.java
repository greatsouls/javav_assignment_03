package com.skynetgate.beans;

import org.springframework.stereotype.Component;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;

/**
 * Created by Emil Gochev.
 */

@Component
public class PlayerContainer {
    @NotNull
    @Size(min=2, max=30)
    private String firstName;

    @NotNull
    @Size(min=2, max=30)
    private String lastName;

    @NotNull
    @Min(value = 20)
    @Max(value = 23)
    private int age;

    @NotNull
    private String countryOfBirth;

    @NotNull
    private PlayerType position;

    @NotNull
    private BigDecimal annualSalary;

    @NotNull
    private int numberOfGoals;

    @NotNull
    private int numberOfBookings;

    public PlayerContainer() {
    }

    private Currency currency = Currency.getInstance(Locale.CANADA);

    public PlayerContainer(String firstName, String lastName, int age, String countryOfBirth, PlayerType position,
                           BigDecimal annualSalary, int numberOfGoals, int numberOfBookings) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.countryOfBirth = countryOfBirth;
        this.position = position;
        this.annualSalary = annualSalary;
        this.numberOfGoals = numberOfGoals;
        this.numberOfBookings = numberOfBookings;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }

    public PlayerType getPosition() {
        return position;
    }

    public void setPosition(PlayerType position) {
        this.position = position;
    }

    public BigDecimal getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(BigDecimal annualSalary) {
        this.annualSalary = annualSalary;
    }

    public int getNumberOfGoals() {
        return numberOfGoals;
    }

    public void setNumberOfGoals(int numberOfGoals) {
        this.numberOfGoals = numberOfGoals;
    }

    public int getNumberOfBookings() {
        return numberOfBookings;
    }

    public void setNumberOfBookings(int numberOfBookings) {
        this.numberOfBookings = numberOfBookings;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlayerContainer)) return false;
        PlayerContainer that = (PlayerContainer) o;
        return Objects.equals(age, that.age) &&
                Objects.equals(numberOfGoals, that.numberOfGoals) &&
                Objects.equals(numberOfBookings, that.numberOfBookings) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(countryOfBirth, that.countryOfBirth) &&
                Objects.equals(position, that.position) &&
                Objects.equals(annualSalary, that.annualSalary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, age, countryOfBirth, position, annualSalary, numberOfGoals, numberOfBookings);
    }

    @Override
    public String toString() {
        return "PlayerContainer{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", countryOfBirth='" + countryOfBirth + '\'' +
                ", position=" + position +
                ", annualSalary=" + annualSalary +
                ", numberOfGoals=" + numberOfGoals +
                ", numberOfBookings=" + numberOfBookings +
                '}';
    }
}
