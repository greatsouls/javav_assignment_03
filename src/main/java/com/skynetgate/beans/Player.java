package com.skynetgate.beans;

/**
 * Created by Emil Gochev.
 */

public class Player {


    private PlayerType position;

    public Player(PlayerType position) {
        this.position = position;
    }

    public PlayerType getPosition() {
        return position;
    }
}
