package com.skynetgate.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.core.type.filter.RegexPatternTypeFilter;

import java.util.regex.Pattern;

/**
 * Created by Emil Gochev.
 */

@Configuration
@Import(DataConfig.class)
@ComponentScan(basePackages = {"com.skynetgate"},
        excludeFilters={
               @Filter(type=FilterType.CUSTOM, value=ClubConfigRoot.WebPackage.class)
        })
public class ClubConfigRoot {

    public static class WebPackage extends RegexPatternTypeFilter {
        public WebPackage() {
            super(Pattern.compile("com\\.skynetgate\\.web"));
        }
    }
}
