create table Player (
 id identity,
 first_name varchar(33) not null,
 last_name varchar(33) not null,
 age int not null,
 country varchar(33) not null,
 position varchar(333) not null,
 salary double not null,
 goals varchar(33) not null,
 bookings varchar(33) not null
);

create table Trainer (
 id identity,
 first_name varchar(33) not null,
 last_name varchar(33) not null,
 age int not null,
 salary double not null
);