<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page session="false" %>
<html>
<head>
    <title>Create Player</title>
    <link rel="stylesheet" type="text/css"
          href="<c:url value="/resources/style.css" />">
</head>
<body>
<h1>Create Player</h1>

<sf:form method="POST" modelAttribute="playerContainer">
    <sf:errors path="*" element="div" cssClass="errors"/>

    <table border="0" cellspacing="2">
        <tr>
            <td><sf:label path="firstName" cssErrorClass="error"> <spring:message code="first.name"/> </sf:label></td>
            <td><sf:input path="firstName" cssErrorClass="error"/><br/></td>
        </tr>
        <tr>
            <td><sf:label path="lastName" cssErrorClass="error"> <spring:message code="last.name"/> </sf:label></td>
            <td><sf:input path="lastName" cssErrorClass="error"/><br/></td>
        </tr>
        <tr>
            <td> <sf:label path="age" cssErrorClass="error"> <spring:message code="age"/> </sf:label></td>
            <td><sf:input path="age" cssErrorClass="error"/><br/></td>
        </tr>
        <tr>
            <td><sf:label path="countryOfBirth" cssErrorClass="error"> <spring:message code="countryOfBirth"/> </sf:label></td>
            <td><select name="countryOfBirth" size="1">
                <option value="Canada">Canada</option>
                <option value="USA">USA</option>
                <option value="Russia">Russia</option>
                <option value="Spain">Spain</option>
            </select></td>
        </tr>
        <tr>
            <td> <sf:label path="position" cssErrorClass="error"> <spring:message code="position"/> </sf:label></td>
            <td><select name="position" size="1">
                <option value="GOALKEEPER">Goalkeeper</option>
                <option value="DEFENDER">Defender</option>
                <option value="MIDFIELDER">Midfielder</option>
                <option value="FORWARD">Forward</option>
            </select></td>
        </tr>
        <tr>
            <td> <sf:label path="annualSalary" cssErrorClass="error"> <spring:message code="annualSalary"/> </sf:label></td>
            <td>  <sf:input path="annualSalary" cssErrorClass="error"/><br/></td>
        </tr>
        <tr>
            <td><sf:label path="numberOfGoals" cssErrorClass="error"> <spring:message code="numberOfGoals"/> </sf:label></td>
            <td> <sf:input path="numberOfGoals" cssErrorClass="error"/><br/></td>
        </tr>
        <tr>
            <td><sf:label path="numberOfBookings" cssErrorClass="error"> <spring:message code="numberOfBookings"/> </sf:label></td>
            <td> <sf:input path="numberOfBookings" cssErrorClass="error"/><br/></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="reset" value="<spring:message code="Reset"/>"/> |
                <input type="submit" value="<spring:message code="Create"/>"/> |
                <button type="button" onclick="document.location='/assignment_03/'"><spring:message code="Cancel"/></button>
            </td>
        </tr>
    </table>
</sf:form>
</body>
</html>
