<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
    <title>Team Management</title>
    <link rel="stylesheet"
          type="text/css"
          href="<c:url value="/resources/style.css" />">
</head>
<body>
<h1>Welcome to Team Management System</h1>
<table>
    <tr>
        <td>
            <a href="<c:url value="/player/create" />">Create player</a> |
        </td>
        <td>
            <a href="<c:url value="/player/displayUsingREST" />">Show Player by last name using REST</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="<c:url value="/trainer/create" />">Create trainer</a> |
        </td>
        <td>
            <a href="<c:url value="/trainer/displayUsingREST" />">Show Trainer by first and last name using REST</a>
        </td>
    </tr>
</table>
</table>
</body>
</html>
