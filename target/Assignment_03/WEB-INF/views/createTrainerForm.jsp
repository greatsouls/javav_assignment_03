<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page session="false" %>
<html>
<head>
    <title>Create Trainer</title>
    <link rel="stylesheet" type="text/css"
          href="<c:url value="/resources/style.css" />">
</head>
<body>
<h1>Create Trainer</h1>
<sf:form method="POST" modelAttribute="trainerContainer">
    <sf:errors path="*" element="div" cssClass="errors"/>
    <table border="0" cellspacing="2">
        <tr>
            <td><sf:label path="firstName"> <spring:message code="first.name"/> </sf:label></td>
            <td><sf:input path="firstName" cssErrorClass="error"/><br/></td>
        </tr>
        <tr>
            <td><sf:label path="lastName"> <spring:message code="last.name"/> </sf:label></td>
            <td><sf:input path="lastName" cssErrorClass="error"/><br/></td>
        </tr>
        <tr>
            <td><sf:label path="age"> <spring:message code="age"/> </sf:label></td>
            <td><sf:input path="age" cssErrorClass="error"/><br/></td>
        </tr>
        <tr>
            <td><sf:label path="annualSalary"> <spring:message code="annualSalary"/> </sf:label></td>
            <td><sf:input path="annualSalary" cssErrorClass="error"/><br/></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="reset" value="<spring:message code="Reset"/>"/> |
                <input type="submit" value="<spring:message code="Create"/>"/> |
                <button type="button" onclick="document.location='/assignment_03/'"><spring:message code="Cancel"/></button>
            </td>
        </tr>
    </table>
</sf:form>
</body>
</html>
