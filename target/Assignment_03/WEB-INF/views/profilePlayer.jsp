<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page session="false" %>
<html>
<head>
    <title>Player Profile</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/style.css" />">
</head>
<body>
<h1>Player Profile</h1>
<table border="0" cellspacing="2" class="playerContainer">
    <c:forEach items="${playerContainer}" var="player">
        <tr>
            <td><spring:message code="first.name"/></td>
            <td><c:out value="${player.firstName}"/><br/></td>
        </tr>
        <tr>
            <td><spring:message code="last.name"/></td>
            <td><c:out value="${player.lastName}"/><br/></td>
        </tr>
        <tr>
            <td><spring:message code="age"/></td>
            <td><c:out value="${player.age}"/><br/></td>
        </tr>
        <tr>
            <td><spring:message code="countryOfBirth"/></td>
            <td><c:out value="${player.countryOfBirth}"/><br/></td>
        </tr>
        <tr>
            <td><spring:message code="position"/></td>
            <td><c:out value="${player.position}"/><br/></td>
        </tr>
        <tr>
            <td><spring:message code="annualSalary"/></td>
            <td><c:out value="${player.annualSalary} ${player.currency}"/><br/></td>
        </tr>
        <tr>
            <td><spring:message code="numberOfGoals"/></td>
            <td><c:out value="${player.numberOfGoals}"/><br/></td>
        </tr>
        <tr>
            <td><spring:message code="numberOfBookings"/></td>
            <td><c:out value="${player.numberOfBookings}"/><br/></td>
        </tr>
        <tr>
            <td colspan="2" align="center">===============================</td>
        </tr>
    </c:forEach>
    <tr>
        <td colspan="2" align="center">
            <button type="button" onclick="document.location='/assignment_03/'"><spring:message code="Return"/></button>
        </td>
    </tr>
</table>
</body>
</html>